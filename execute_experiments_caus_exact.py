#!/usr/bin/env python

# This script runs the experiments and prints the result of each query in a file:
#  - for exact inference:
#  - for approximate inference
# and the avereges are printed in a CSV file

import os
import shlex
import shutil
import subprocess
import random
import csv
import threading
import signal

DIM_GLOBAL_STACK = "8g"
GRAPH_EXACT_DIR = "./graphs/exact"
GRAPH_MCINTYRE_DIR = "./graphs/mcintyre"
GRAPH_BASENAME = "graph_"
OUTPUT_FILEBASENAME = "result_"
EXACT_DIR = "./exact"
MCINTYRE_DIR = "./mcintyre"
MCINTYRE_SAMPLES = 1000
MCINTYRE_LAG = 1
MAX_DIM_GRAPH = 100
MAX_DIM_GRAPH_STEP = 10
MAX_NUM_GRAPH = 10
MAX_NUM_EVIDENCE = 8
START_NUM_EVIDENCE = 2
MAX_NUM_QUERIES = 10
SEED = 1234

random.seed(SEED)

QUERIES_TIMEOUT = 600

def sig_term(*args, **kwargs):
#    kill_subprocesses()
    sys.exit(signal.SIGTERM | 0x80)

signal.signal(signal.SIGTERM, sig_term)

class SubprocessTimeoutError(RuntimeError):
  """Error class for subprocess timeout."""
  pass


class ExecutionThread(threading.Thread):
    def __init__(self, cmd):
        self.stdout = None
        self.stderr = None
        self.cmd = cmd
        self.p = None
        self._kill = threading.Event()
        threading.Thread.__init__(self)

    def run(self):
        self.p = subprocess.Popen(self.cmd,
                             shell=False,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

        self.stdout, self.stderr = self.p.communicate()
        
    def kill(self):
        try:
            self.p.kill()
        except OSError, e:
            # The process finished between the `is_alive()` and `kill()`
            #print self.stdout
            return
#            return self.stdout



class QueriesThread(threading.Thread):
    def __init__(self, x, y):
        threading.Thread.__init__(self)
        self.x = x
        self.y = y
        self._stop = threading.Event()
        
    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()
    
    def run(self):
        self.run_queries()

    def run_queries(self):
        y = self.y
        x = self.x
        for ev_size in range(START_NUM_EVIDENCE, MAX_NUM_EVIDENCE + 1, 2):
            print "dimension: %d num evidences: %d" % (n, ev_size)
            total_nodes = range(1, n + 1)
            caus_exact_query_file = open(os.path.join(dir_n_graph_exact, OUTPUT_FILEBASENAME + "caus_" + str(ev_size)), "w")
            files = [caus_exact_query_file]
            for _ in xrange(MAX_NUM_QUERIES):
                evidence_nodes = random.sample(total_nodes, ev_size)
                rest = list(set(total_nodes) - set(evidence_nodes))
                atom_node = random.sample(rest, 1)[0]
                evidence_literals = ""
                evidence_do_literals = ""
                for node in evidence_nodes:
                    evidence_literals += ("has(%s)," % node)
                    evidence_do_literals += ("do(has(%s))," % node)
                evidence_literals = evidence_literals[:-1]
                evidence_do_literals = evidence_do_literals[:-1]
                atom = ("has(%s)" % atom_node)
                for j in range(1, MAX_NUM_GRAPH + 1):
                    if not self.stopped():
                        graph_exact_file = os.path.join(GRAPH_EXACT_DIR, str(n), GRAPH_BASENAME + str(j))
                        graph_mcintyre_file = os.path.join(GRAPH_MCINTYRE_DIR, str(n), GRAPH_BASENAME + str(j))

                        start_exact = "swipl -s %s --quiet -G%s -g \"statistics(walltime, [Start, _])," % (graph_exact_file, DIM_GLOBAL_STACK)
                        end = "statistics(walltime, [End,_]),CPUT is End - Start,writeln(CPUT),writeln(P),halt.\""

                        caus_query = "prob(%s,(%s),P)," % (atom, evidence_do_literals)
                        caus_exact_query = start_exact + caus_query + end
                        print "%s" % (caus_exact_query)
                        args = shlex.split(caus_exact_query)
        #                    subprocess.call(args, stdout=caus_exact_query_file)
#                        out = subprocess.check_output(args)
                        try:
                            if not self.stopped():
                                out = run_command_with_timeout(args, QUERIES_TIMEOUT)
                                caus_exact_query_file.write(out.split()[0]+"\n")
                                caus_exact_query_file.flush()
                                caus_exact_matrix[x][y] = caus_exact_matrix[x][y] + float(out.split()[0])
                                print "Time: %s" % (out.split()[0])
                                print "Prob: %s\n" % (out.split()[1])
                            else:
                                #print "query %s with graph %d of dim %d and # of literals %d: timeout\n" % (caus_exact_query, j, (x+1)*MAX_DIM_GRAPH_STEP, ev_size)
                                print "timeout\n"
                                caus_exact_query_file.write("timeout\n")
                                caus_exact_query_file.flush()
                                caus_exact_matrix[x][y] = float('inf')
                        except SubprocessTimeoutError:
                            #print "query %s with graph %d of dim %d and # of literals %d: timeout\n" % (caus_exact_query, j, (x+1)*MAX_DIM_GRAPH_STEP, ev_size)
                            print "timeout\n"
                            caus_exact_query_file.write("timeout\n")
                            caus_exact_query_file.flush()
                            caus_exact_matrix[x][y] = float('inf')

                    else:
                        caus_exact_matrix[x][y] = float('inf')
                        for f in files:
                            f.write("timeout\n")
                            f.flush()
                for f in files:
                    f.flush()
                    f.write("\n")
                    f.flush()
            for f in files:
    #                f.write("\n\n")
                f.close()

            y = y +1
            
def run_command_with_timeout(cmd, timeout_sec):
    """Execute `cmd` in a subprocess and enforce timeout `timeout_sec` seconds.
 
    Return subprocess exit code on natural completion of the subprocess.
    Raise an exception if timeout expires before subprocess completes."""
    proc_thread = ExecutionThread(cmd)
    proc_thread.start()
    proc_thread.join(timeout_sec)
    if proc_thread.is_alive():
        proc_thread.kill()
        # OK, the process was definitely killed
        raise SubprocessTimeoutError('Process #%d killed after %f seconds' % (proc_thread.p.pid, timeout_sec))
    # Process completed naturally - return exit code
    #print proc_thread.stdout
    return proc_thread.stdout

if __name__ == "__main__":
    random.seed(SEED)
    
    if not os.path.exists(EXACT_DIR):
#        shutil.rmtree(EXACT_DIR)
        os.makedirs(EXACT_DIR)
    if not os.path.exists(MCINTYRE_DIR):
#        shutil.rmtree(MCINTYRE_DIR)
        os.makedirs(MCINTYRE_DIR)

    #caus_exact_prob_file = open(os.path.join("caus_exact_prob" + str(ev_size)), "w")
    caus_exact_matrix = [[0.0 for x in xrange((MAX_NUM_EVIDENCE - START_NUM_EVIDENCE)/2 + 1) ] for y in xrange((MAX_DIM_GRAPH-10)/MAX_DIM_GRAPH_STEP + 1)]
    x = 0
    for n in range(10, MAX_DIM_GRAPH + 1, MAX_DIM_GRAPH_STEP):
        # creating directory containing graphs of n size
        dir_n_graph_exact = os.path.join(EXACT_DIR, str(n))
        # if the directory containing the results exists, delete it
#        if os.path.exists(dir_n_graph_exact):
#            shutil.rmtree(dir_n_graph_exact)
#        os.makedirs(dir_n_graph_exact)
        if not os.path.exists(dir_n_graph_exact):
            os.makedirs(dir_n_graph_exact)
        y = 0
        run_queries_thread = QueriesThread(x,y)
        run_queries_thread.start()
        run_queries_thread.join(QUERIES_TIMEOUT)
        if run_queries_thread.isAlive():
            run_queries_thread.stop()
            run_queries_thread.join()
        x = x + 1
        
        # EXAMPLE
        #echo "CPU is cputime,prob(has(1), (has(2), has(3), has(67), has(75), has(67), has(48)),P),CPU2 is cputime,CPUT is CPU2 - CPU,print(CPUT)." | swipl -s graph_1
    
    result_matrices = [caus_exact_matrix]
    # compute the avarages
    divisor = MAX_NUM_QUERIES * MAX_NUM_GRAPH
    for result_matrix in result_matrices:
        for row_i in xrange(len(result_matrix)):
            for col_j in xrange(len(result_matrix[row_i])):
                result_matrix[row_i][col_j] = result_matrix[row_i][col_j] / divisor
    
    # Write results on a CSV file
    dimensions = []
    for n in range(10, MAX_DIM_GRAPH + 1, MAX_DIM_GRAPH_STEP):
        dimensions.append(n)
        
    evidence_sizes = [' ']
    for ev_size in range(START_NUM_EVIDENCE, MAX_NUM_EVIDENCE + 1, 2):
        evidence_sizes.append(ev_size)
    
    files = ("caus_exact_%d-%d.csv " \
        % ((START_NUM_EVIDENCE, MAX_NUM_EVIDENCE) * 1)).split()
        
    for filename,result_matrix in zip(files, result_matrices):
        with open(filename, "w") as fp:
            wr = csv.writer(fp, dialect='excel')
            wr.writerow(evidence_sizes)
            for row,n in zip(result_matrix, range(10, MAX_DIM_GRAPH + 1, MAX_DIM_GRAPH_STEP)):
                l = [n]
                l.extend(row)
                wr.writerow(l)


