#!/usr/bin/env python

# This script runs the experiments and prints the result of each query in a file:
#  - for exact inference:
#  - for approximate inference
# and the avereges are printed in a CSV file

DIM_GLOBAL_STACK = "3g"

if __name__ == "__main__":
    import os
    import shlex
    import shutil
    import subprocess
    import random
    import csv

    GRAPH_EXACT_DIR = "./graphs/exact"
    GRAPH_MCINTYRE_DIR = "./graphs/mcintyre"
    GRAPH_BASENAME = "graph_"
    OUTPUT_FILEBASENAME = "result_"
    EXACT_DIR = "./exact_opt"
    MCINTYRE_DIR = "./mcintyre_opt"
    MCINTYRE_SAMPLES = 1000
    MCINTYRE_LAG = 1
    MAX_DIM_GRAPH = 20
    MAX_DIM_GRAPH_STEP = 10
    MAX_NUM_GRAPH = 10
    MAX_NUM_EVIDENCE = 2
    START_NUM_EVIDENCE = 2
    MAX_NUM_QUERIES = 10
    SEED = 1234

    random.seed(SEED)
    
    if not os.path.exists(EXACT_DIR):
#        shutil.rmtree(EXACT_DIR)
        os.makedirs(EXACT_DIR)
    if not os.path.exists(MCINTYRE_DIR):
#        shutil.rmtree(MCINTYRE_DIR)
        os.makedirs(MCINTYRE_DIR)

    cond_exact_matrix = [[0.0 for x in xrange((MAX_NUM_EVIDENCE - START_NUM_EVIDENCE)/2 + 1) ] for y in xrange((MAX_DIM_GRAPH-10)/MAX_DIM_GRAPH_STEP + 1)]
    caus_exact_matrix = [[0.0 for x in xrange((MAX_NUM_EVIDENCE - START_NUM_EVIDENCE)/2 + 1) ] for y in xrange((MAX_DIM_GRAPH-10)/MAX_DIM_GRAPH_STEP + 1)]
    cond_mcintyre_mh_matrix = [[0.0 for x in xrange((MAX_NUM_EVIDENCE -START_NUM_EVIDENCE)/2 + 1) ] for y in xrange((MAX_DIM_GRAPH-10)/MAX_DIM_GRAPH_STEP + 1)]
    caus_mcintyre_mh_matrix = [[0.0 for x in xrange((MAX_NUM_EVIDENCE - START_NUM_EVIDENCE)/2 + 1) ] for y in xrange((MAX_DIM_GRAPH-10)/MAX_DIM_GRAPH_STEP + 1)]
    x = 0
    for n in range(10, MAX_DIM_GRAPH + 1, MAX_DIM_GRAPH_STEP):
        # creating directory containing graphs of n size
        dir_n_graph_exact = os.path.join(EXACT_DIR, str(n))
        dir_n_graph_mcintyre = os.path.join(MCINTYRE_DIR, str(n))
        # if the directory containing the results exists, delete it
        if os.path.exists(dir_n_graph_exact):
            shutil.rmtree(dir_n_graph_exact)
        os.makedirs(dir_n_graph_exact)
        if os.path.exists(dir_n_graph_mcintyre):
            shutil.rmtree(dir_n_graph_mcintyre)
        os.makedirs(dir_n_graph_mcintyre)
        y = 0
        for ev_size in range(START_NUM_EVIDENCE, MAX_NUM_EVIDENCE + 1, 2):
            print "dimension: %d num evidences: %d" % (n, ev_size)
            total_nodes = range(1, n + 1)
            cond_exact_query_file = open(os.path.join(dir_n_graph_exact, OUTPUT_FILEBASENAME + "cond_" + str(ev_size)), "w")
#            cond_mcintyre_rejection_query_file = open(os.path.join(dir_n_graph_mcintyre, OUTPUT_FILEBASENAME + "cond_rejection_" + str(ev_size)), "a")
            cond_mcintyre_mh_query_file = open(os.path.join(dir_n_graph_mcintyre, OUTPUT_FILEBASENAME + "cond_mh_" + str(ev_size)), "w")
            caus_exact_query_file = open(os.path.join(dir_n_graph_exact, OUTPUT_FILEBASENAME + "caus_" + str(ev_size)), "w")
#            caus_mcintyre_rejection_query_file = open(os.path.join(dir_n_graph_mcintyre, OUTPUT_FILEBASENAME + "caus_rejection_" + str(ev_size)), "a")
            caus_mcintyre_mh_query_file = open(os.path.join(dir_n_graph_mcintyre, OUTPUT_FILEBASENAME + "caus_mh_" + str(ev_size)), "w")
#            files = [cond_exact_query_file, caus_exact_query_file, cond_mcintyre_rejection_query_file, cond_mcintyre_mh_query_file, caus_mcintyre_rejection_query_file, caus_mcintyre_mh_query_file]
            files = [cond_exact_query_file, caus_exact_query_file, cond_mcintyre_mh_query_file, caus_mcintyre_mh_query_file]
            cond_exact_query = ""
            caus_exact_query = ""
            cond_mcintyre_mh_query = ""
            caus_mcintyre_mh_query = ""
            for _ in xrange(MAX_NUM_QUERIES):
                evidence_nodes = random.sample(total_nodes, ev_size)
                rest = list(set(total_nodes) - set(evidence_nodes))
                atom_node = random.sample(rest, 1)[0]
                evidence_literals = ""
                evidence_do_literals = ""
                for node in evidence_nodes:
                    evidence_literals += ("has(%s)," % node)
                    evidence_do_literals += ("do(has(%s))," % node)
                evidence_literals = evidence_literals[:-1]
                evidence_do_literals = evidence_do_literals[:-1]
                atom = ("has(%s)" % atom_node)
                cond_exact_query += "prob(%s,(%s),_)," % (atom, evidence_literals)
                caus_exact_query += "prob(%s,(%s),_)," % (atom, evidence_do_literals)
                cond_mcintyre_mh_query +=  "mc_mh_sample(%s,(%s),%d,%d,_,_,_)," \
                    % (atom, evidence_literals, MCINTYRE_SAMPLES, MCINTYRE_LAG)
                caus_mcintyre_mh_query += "mc_mh_sample(%s,(%s),%d,%d,_,_,_)," \
                    % (atom, evidence_do_literals, MCINTYRE_SAMPLES, MCINTYRE_LAG)
            for j in range(1, MAX_NUM_GRAPH + 1):
                graph_exact_file = os.path.join(GRAPH_EXACT_DIR, str(n), GRAPH_BASENAME + str(j))
                graph_mcintyre_file = os.path.join(GRAPH_MCINTYRE_DIR, str(n), GRAPH_BASENAME + str(j))

#                start_exact = ("swipl -s %s --quiet -g \"CPU is cputime," % graph_exact_file)
#                start_mcintyre = ("swipl -s %s --quiet -g \"CPU is cputime," % graph_mcintyre_file)
#                end = "CPU2 is cputime,CPUT is CPU2 - CPU,writeln(CPUT),halt.\""
                start_exact = "swipl -s %s --quiet -G%s -g \"statistics(walltime, [Start, _])," % (graph_exact_file, DIM_GLOBAL_STACK)
                start_mcintyre = "swipl -s %s --quiet -G%s -g \"statistics(walltime, [Start,_])," % (graph_mcintyre_file, DIM_GLOBAL_STACK)
                end = "statistics(walltime, [End,_]),CPUT is End - Start,writeln(CPUT),halt.\""

                command = start_exact + cond_exact_query + end
                args = shlex.split(command)
#                    subprocess.call(args, stdout=cond_exact_query_file)
                out = subprocess.check_output(args)
                cond_exact_query_file.write("query: " + cond_exact_query + "\n")
                cond_exact_query_file.write(out)
                cond_exact_query_file.flush()
                cond_exact_matrix[x][y] = cond_exact_matrix[x][y] + float(out)

                # In order to perform cnditioned queries with MCINTYRE you have to use rejection or Metropolis-Hastings Markov Chain
#                    cond_mcintyre_rejection_query = start_mcintyre + "mc_rejection_sample(%s,(%s),%s,_,_,_)," % (atom, evidence_literals, str(MCINTYRE_SAMPLES)) + end
#                    args = shlex.split(cond_mcintyre_rejection_query)
#                    subprocess.call(args, stdout=cond_mcintyre_rejection_query_file)

                command = start_exact + caus_exact_query + end
                args = shlex.split(command)
#                    subprocess.call(args, stdout=caus_exact_query_file)
                out = subprocess.check_output(args)
                caus_exact_query_file.write("query: " + caus_exact_query + "\n")
                caus_exact_query_file.write(out)
                caus_exact_query_file.flush()
                caus_exact_matrix[x][y] = caus_exact_matrix[x][y] + float(out)

#                    caus_mcintyre_rejection_query = start_mcintyre + "mc_rejection_sample(%s,(%s),%s,_,_,_)," % (atom, evidence_do_literals, str(MCINTYRE_SAMPLES)) + end
#                    args = shlex.split(caus_mcintyre_rejection_query)
#                    subprocess.call(args, stdout=caus_mcintyre_rejection_query_file)

                command = start_mcintyre + cond_mcintyre_mh_query + end
                args = shlex.split(command)
#                    subprocess.call(args, stdout=cond_mcintyre_mh_query_file)
                out = subprocess.check_output(args)
                cond_mcintyre_mh_query_file.write("query: " + cond_mcintyre_mh_query + "\n")
                cond_mcintyre_mh_query_file.write(out)
                cond_mcintyre_mh_query_file.flush()
                cond_mcintyre_mh_matrix[x][y] = cond_mcintyre_mh_matrix[x][y] + float(out)

                command = start_mcintyre + caus_mcintyre_mh_query + end
                args = shlex.split(command)
#                    subprocess.call(args, stdout=caus_mcintyre_mh_query_file)
                out = subprocess.check_output(args)
                caus_mcintyre_mh_query_file.write("query: " + caus_mcintyre_mh_query + "\n")
                caus_mcintyre_mh_query_file.write(out)
                caus_mcintyre_mh_query_file.flush()
                caus_exact_query_file.flush()
                caus_mcintyre_mh_matrix[x][y] = caus_mcintyre_mh_matrix[x][y] + float(out)
                for f in files:
                    f.flush()
                    f.write("\n")
                    f.flush()
            for f in files:
#                f.write("\n\n")
                f.close()
                
            y = y +1
        x = x + 1
        
    #                do_mcintyre_query = str1 + ("mc_prob(%s,(%s),P)," % atom, evidence_do_literals) 
    #                + str2 + ("| swipl -s %s" % graph_mcintyre_file)
        # EXAMPLE
        #echo "CPU is cputime,prob(has(1), (has(2), has(3), has(67), has(75), has(67), has(48)),P),CPU2 is cputime,CPUT is CPU2 - CPU,print(CPUT)." | swipl -s graph_1
    
    result_matrices = [cond_exact_matrix, caus_exact_matrix, cond_mcintyre_mh_matrix, caus_mcintyre_mh_matrix]
    # compute the avarages
    divisor = MAX_NUM_QUERIES * MAX_NUM_GRAPH
    for result_matrix in result_matrices:
        for row_i in xrange(len(result_matrix)):
            for col_j in xrange(len(result_matrix[row_i])):
                result_matrix[row_i][col_j] = result_matrix[row_i][col_j] / divisor
    
    # Write results on a CSV file
    dimensions = []
    for n in range(10, MAX_DIM_GRAPH + 1, MAX_DIM_GRAPH_STEP):
        dimensions.append(n)
        
    evidence_sizes = [' ']
    for ev_size in range(START_NUM_EVIDENCE, MAX_NUM_EVIDENCE + 1, 2):
        evidence_sizes.append(ev_size)
    
    files = ("cond_exact_opt_%d-%d.csv caus_exact_opt_%d-%d.csv " \
        % ((START_NUM_EVIDENCE, MAX_NUM_EVIDENCE) * 2)
        + "cond_mcintyre_mh_opt_%d-%d.csv caus_mcintyre_mh_opt_%d-%d.csv" \
        % ((START_NUM_EVIDENCE, MAX_NUM_EVIDENCE) * 2)).split()
#    i = 0
    for filename,result_matrix in zip(files, result_matrices):
#        if "exact" in filename:
#            filename = os.path.join(EXACT_DIR, filename)
#        elif "mcintyre" in filename:
#            filename = os.path.join(MCINTYRE_DIR, filename)
        with open(filename, "w") as fp:
            wr = csv.writer(fp, dialect='excel')
            wr.writerow(evidence_sizes)
            #n = 0
#            for row in result_matrices[i]:
            for row,n in zip(result_matrix, range(10, MAX_DIM_GRAPH + 1, MAX_DIM_GRAPH_STEP)):
                l = [n]
                l.extend(row)
                wr.writerow(l)
             #   n = n + 1
#        i = i + 1
