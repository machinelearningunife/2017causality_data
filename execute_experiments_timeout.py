#!/usr/bin/env python

# This script runs the experiments and prints the result of each query in a file:
#  - for exact inference:
#  - for approximate inference
# and the avereges are printed in a CSV file

import os
import shlex
import shutil
import subprocess
import random
import csv
import threading
import signal

DIM_GLOBAL_STACK = "3g"
GRAPH_EXACT_DIR = "./graphs/exact"
GRAPH_MCINTYRE_DIR = "./graphs/mcintyre"
GRAPH_BASENAME = "graph_"
OUTPUT_FILEBASENAME = "result_"
EXACT_DIR = "./exact_t"
MCINTYRE_DIR = "./mcintyre_t"
MCINTYRE_SAMPLES = 1000
MCINTYRE_LAG = 1
MAX_DIM_GRAPH = 20
MAX_DIM_GRAPH_STEP = 10
MAX_NUM_GRAPH = 10
MAX_NUM_EVIDENCE = 4
START_NUM_EVIDENCE = 2
MAX_NUM_QUERIES = 10
SEED = 1234

random.seed(SEED)

QUERIES_TIMEOUT = 600

def sig_term(*args, **kwargs):
#    kill_subprocesses()
    sys.exit(signal.SIGTERM | 0x80)

signal.signal(signal.SIGTERM, sig_term)

class SubprocessTimeoutError(RuntimeError):
  """Error class for subprocess timeout."""
  pass


class ExecutionThread(threading.Thread):
    def __init__(self, cmd):
        self.stdout = None
        self.stderr = None
        self.cmd = cmd
        self.p = None
        self._kill = threading.Event()
        threading.Thread.__init__(self)

    def run(self):
        self.p = subprocess.Popen(self.cmd,
                             shell=False,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

        self.stdout, self.stderr = self.p.communicate()
        
    def kill(self):
        try:
            self.p.kill()
        except OSError, e:
            # The process finished between the `is_alive()` and `kill()`
            return



class QueriesThread(threading.Thread):
    def __init__(self, x, y):
        threading.Thread.__init__(self)
        self.x = x
        self.y = y
        self._stop = threading.Event()
        
    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()
    
    def run(self):
        self.run_queries()

    def run_queries(self):
        y = self.y
        x = self.x
        for ev_size in range(START_NUM_EVIDENCE, MAX_NUM_EVIDENCE + 1, 2):
            print "dimension: %d num evidences: %d" % (n, ev_size)
            total_nodes = range(1, n + 1)
            cond_exact_query_file = open(os.path.join(dir_n_graph_exact, OUTPUT_FILEBASENAME + "cond_" + str(ev_size)), "w")
            cond_mcintyre_mh_query_file = open(os.path.join(dir_n_graph_mcintyre, OUTPUT_FILEBASENAME + "cond_mh_" + str(ev_size)), "w")
            caus_exact_query_file = open(os.path.join(dir_n_graph_exact, OUTPUT_FILEBASENAME + "caus_" + str(ev_size)), "w")
            caus_mcintyre_mh_query_file = open(os.path.join(dir_n_graph_mcintyre, OUTPUT_FILEBASENAME + "caus_mh_" + str(ev_size)), "w")
            files = [cond_exact_query_file, caus_exact_query_file, cond_mcintyre_mh_query_file, caus_mcintyre_mh_query_file]
            for _ in xrange(MAX_NUM_QUERIES):
                evidence_nodes = random.sample(total_nodes, ev_size)
                rest = list(set(total_nodes) - set(evidence_nodes))
                atom_node = random.sample(rest, 1)[0]
                evidence_literals = ""
                evidence_do_literals = ""
                for node in evidence_nodes:
                    evidence_literals += ("has(%s)," % node)
                    evidence_do_literals += ("do(has(%s))," % node)
                evidence_literals = evidence_literals[:-1]
                evidence_do_literals = evidence_do_literals[:-1]
                atom = ("has(%s)" % atom_node)
                # TO DO: modificare questa parte!! Il timeout deve riguardare solo un
                # tipo di query tra cond_exact, caus_exact, cond_mh e caus_mh
                for j in range(1, MAX_NUM_GRAPH + 1):
                    if not self.stopped():
                        graph_exact_file = os.path.join(GRAPH_EXACT_DIR, str(n), GRAPH_BASENAME + str(j))
                        graph_mcintyre_file = os.path.join(GRAPH_MCINTYRE_DIR, str(n), GRAPH_BASENAME + str(j))

                        start_exact = "swipl -s %s --quiet -G%s -g \"statistics(walltime, [Start, _])," % (graph_exact_file, DIM_GLOBAL_STACK)
                        start_mcintyre = "swipl -s %s --quiet -G%s -g \"statistics(walltime, [Start,_])," % (graph_mcintyre_file, DIM_GLOBAL_STACK)
                        end = "statistics(walltime, [End,_]),CPUT is End - Start,writeln(CPUT),halt.\""

                        cond_exact_query = start_exact + "prob(%s,(%s),P)," % (atom, evidence_literals) + end
                        args = shlex.split(cond_exact_query)
                        try:
                            if not self.stopped():
                                out = run_command_with_timeout(args, QUERIES_TIMEOUT)
                                cond_exact_query_file.write(out)
                                cond_exact_query_file.flush()
                                cond_exact_matrix[x][y] = cond_exact_matrix[x][y] + float(out)
                            else:
                                cond_exact_query_file.write("timeout")
                                cond_exact_query_file.flush()
                                cond_exact_matrix[x][y] = float('inf')
                        except SubprocessTimeoutError:
                            print "query %s with graph %d of dim %d and # of literals %d: timeout\n" % (cond_exact_query, j, (x+1)*MAX_DIM_GRAPH_STEP, ev_size)
                            cond_exact_query_file.write("timeout")
                            cond_exact_query_file.flush()
                            cond_exact_matrix[x][y] = float('inf')
                        

                        # In order to perform cnditioned queries with MCINTYRE you have to use rejection or Metropolis-Hastings Markov Chain
                        cond_mcintyre_mh_query = start_mcintyre + "mc_mh_sample(%s,(%s),%s,%s,_,_,_)," % (atom, evidence_literals, str(MCINTYRE_SAMPLES), str(MCINTYRE_LAG)) + end
                        args = shlex.split(cond_mcintyre_mh_query)
                        try:
                            if not self.stopped():
                                out = run_command_with_timeout(args, QUERIES_TIMEOUT)
                                cond_mcintyre_mh_query_file.write(out)
                                cond_mcintyre_mh_query_file.flush()
                                cond_mcintyre_mh_matrix[x][y] = cond_mcintyre_mh_matrix[x][y] + float(out)
                            else:
                                cond_mcintyre_mh_query_file.write("timeout")
                                cond_mcintyre_mh_query_file.flush()
                                cond_mcintyre_mh_matrix[x][y] = float('inf')
                        except SubprocessTimeoutError:
                            print "query %s with graph %d of dim %d and # of literals %d: timeout\n" % (cond_mcintyre_mh_query, j, (x+1)*MAX_DIM_GRAPH_STEP, ev_size)
                            cond_mcintyre_mh_query_file.write("timeout")
                            cond_mcintyre_mh_query_file.flush()
                            cond_mcintyre_mh_matrix[x][y] = float('inf')

                        caus_exact_query = start_exact + "prob(%s,(%s),P)," % (atom, evidence_do_literals) + end
                        args = shlex.split(caus_exact_query)
                        try:
                            if not self.stopped():
                                out = run_command_with_timeout(args, QUERIES_TIMEOUT)
                                caus_exact_query_file.write(out)
                                caus_exact_query_file.flush()
                                caus_exact_matrix[x][y] = caus_exact_matrix[x][y] + float(out)
                            else:
                                caus_exact_query_file.write("timeout")
                                caus_exact_query_file.flush()
                                caus_exact_matrix[x][y] = float('inf')
                        except SubprocessTimeoutError:
                            print "query %s with graph %d of dim %d and # of literals %d: timeout\n" % (caus_exact_query, j, (x+1)*MAX_DIM_GRAPH_STEP, ev_size)
                            caus_exact_query_file.write("timeout")
                            caus_exact_query_file.flush()
                            caus_exact_matrix[x][y] = float('inf')

                        caus_mcintyre_mh_query = start_mcintyre + "mc_mh_sample(%s,(%s),%s,%s,_,_,_)," % (atom, evidence_do_literals, str(MCINTYRE_SAMPLES), str(MCINTYRE_LAG)) + end
                        args = shlex.split(caus_mcintyre_mh_query)
                        try:
                            if not self.stopped():
                                out = run_command_with_timeout(args, QUERIES_TIMEOUT)
                                caus_mcintyre_mh_query_file.write(out)
                                caus_mcintyre_mh_query_file.flush()
                                caus_mcintyre_mh_matrix[x][y] = caus_mcintyre_mh_matrix[x][y] + float(out)
                            else:
                                caus_mcintyre_mh_query_file.write("timeout")
                                caus_mcintyre_mh_query_file.flush()
                                caus_mcintyre_mh_matrix[x][y] = float('inf')
                        except SubprocessTimeoutError:
                            print "query %s with graph %d of dim %d and # of literals %d: timeout\n" % (caus_mcintyre_mh_query, j, (x+1)*MAX_DIM_GRAPH_STEP, ev_size)
                            caus_mcintyre_mh_query_file.write("timeout")
                            caus_mcintyre_mh_query_file.flush()
                            caus_mcintyre_mh_matrix[x][y] = float('inf')
                    else:
                        # TO DO: modificare questa parte devo aggiornare tutte le matrici!! Inoltre il timeout deve riguardare solo un
                        # tipo di query tra cond_exact, caus_exact, cond_mh e caus_mh
                        cond_exact_matrix[x][y] = float('inf')
                        for f in files:
                            f.write("timeout")
                            f.flush()
                for f in files:
                    f.flush()
                    f.write("\n")
                    f.flush()
            for f in files:
    #                f.write("\n\n")
                f.close()

            y = y +1
            
def run_command_with_timeout(cmd, timeout_sec):
    """Execute `cmd` in a subprocess and enforce timeout `timeout_sec` seconds.
 
    Return subprocess exit code on natural completion of the subprocess.
    Raise an exception if timeout expires before subprocess completes."""
    proc_thread = ExecutionThread(cmd)
    proc_thread.start()
    proc_thread.join(timeout_sec)
    if proc_thread.is_alive():
        proc_thread.kill()
        # OK, the process was definitely killed
        raise SubprocessTimeoutError('Process #%d killed after %f seconds' % (proc_thread.p.pid, timeout_sec))
    # Process completed naturally - return exit code
    #print proc_thread.stdout
    return proc_thread.stdout

if __name__ == "__main__":
    random.seed(SEED)
    
    if not os.path.exists(EXACT_DIR):
#        shutil.rmtree(EXACT_DIR)
        os.makedirs(EXACT_DIR)
    if not os.path.exists(MCINTYRE_DIR):
#        shutil.rmtree(MCINTYRE_DIR)
        os.makedirs(MCINTYRE_DIR)

    cond_exact_matrix = [[0.0 for x in xrange((MAX_NUM_EVIDENCE - START_NUM_EVIDENCE)/2 + 1) ] for y in xrange((MAX_DIM_GRAPH-10)/MAX_DIM_GRAPH_STEP + 1)]
    caus_exact_matrix = [[0.0 for x in xrange((MAX_NUM_EVIDENCE - START_NUM_EVIDENCE)/2 + 1) ] for y in xrange((MAX_DIM_GRAPH-10)/MAX_DIM_GRAPH_STEP + 1)]
    cond_mcintyre_mh_matrix = [[0.0 for x in xrange((MAX_NUM_EVIDENCE -START_NUM_EVIDENCE)/2 + 1) ] for y in xrange((MAX_DIM_GRAPH-10)/MAX_DIM_GRAPH_STEP + 1)]
    caus_mcintyre_mh_matrix = [[0.0 for x in xrange((MAX_NUM_EVIDENCE - START_NUM_EVIDENCE)/2 + 1) ] for y in xrange((MAX_DIM_GRAPH-10)/MAX_DIM_GRAPH_STEP + 1)]
    x = 0
    for n in range(10, MAX_DIM_GRAPH + 1, MAX_DIM_GRAPH_STEP):
        # creating directory containing graphs of n size
        dir_n_graph_exact = os.path.join(EXACT_DIR, str(n))
        dir_n_graph_mcintyre = os.path.join(MCINTYRE_DIR, str(n))
        # if the directory containing the results exists, delete it
        if os.path.exists(dir_n_graph_exact):
            shutil.rmtree(dir_n_graph_exact)
        os.makedirs(dir_n_graph_exact)
        if os.path.exists(dir_n_graph_mcintyre):
            shutil.rmtree(dir_n_graph_mcintyre)
        os.makedirs(dir_n_graph_mcintyre)
        y = 0
        run_queries_thread = QueriesThread(x,y)
        run_queries_thread.start()
        run_queries_thread.join(QUERIES_TIMEOUT)
        if run_queries_thread.isAlive():
            run_queries_thread.stop()
        run_queries_thread.join()
        x = x + 1
        
        # EXAMPLE
        #echo "CPU is cputime,prob(has(1), (has(2), has(3), has(67), has(75), has(67), has(48)),P),CPU2 is cputime,CPUT is CPU2 - CPU,print(CPUT)." | swipl -s graph_1
    
    result_matrices = [cond_exact_matrix, caus_exact_matrix, cond_mcintyre_mh_matrix, caus_mcintyre_mh_matrix]
    # compute the avarages
    divisor = MAX_NUM_QUERIES * MAX_NUM_GRAPH
    for result_matrix in result_matrices:
        for row_i in xrange(len(result_matrix)):
            for col_j in xrange(len(result_matrix[row_i])):
                result_matrix[row_i][col_j] = result_matrix[row_i][col_j] / divisor
    
    # Write results on a CSV file
    dimensions = []
    for n in range(10, MAX_DIM_GRAPH + 1, MAX_DIM_GRAPH_STEP):
        dimensions.append(n)
        
    evidence_sizes = [' ']
    for ev_size in range(START_NUM_EVIDENCE, MAX_NUM_EVIDENCE + 1, 2):
        evidence_sizes.append(ev_size)
        
    files = ("cond_exact_t_%d-%d.csv caus_exact_t_%d-%d.csv " \
        % ((START_NUM_EVIDENCE, MAX_NUM_EVIDENCE) * 2)
        + "cond_mcintyre_mh_t_%d-%d.csv caus_mcintyre_mh_t_%d-%d.csv" \
        % ((START_NUM_EVIDENCE, MAX_NUM_EVIDENCE) * 2)).split()
    for filename,result_matrix in zip(files, result_matrices):
        with open(filename, "w") as fp:
            wr = csv.writer(fp, dialect='excel')
            wr.writerow(evidence_sizes)
            for row,n in zip(result_matrix, range(10, MAX_DIM_GRAPH + 1, MAX_DIM_GRAPH_STEP)):
                l = [n]
                l.extend(row)
                wr.writerow(l)


