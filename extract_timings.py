import os
import re
import csv

MAX_NUM_EVIDENCE = 8
MIN_NUM_EVIDENCE = 2
STEP_NUM_EVIDENCE = 2
MAX_DIM_GRAPH = 100
STEP_DIM_GRAPH = 10
MIN_DIM_GRAPH = 10

def extract_timings(dest_dir, filepath, prefix_dest_file):
    if not os.path.exists(dest_dir):
        os.mkdir(dest_dir)
    
    # read file that contains the exact results
    with open(filepath, 'r') as file:
        line = file.readline()
        match_obj = re.match("dimension: (\d+) num evidences: (\d+)", line)
        dim = int(match_obj.group(1))
        dir_n_graph = os.path.join(dest_dir, str(dim))
        if not os.path.exists(dir_n_graph):
            os.makedirs(dir_n_graph)
            
        evidences = int(match_obj.group(2))
        
        dest_file = open(os.path.join(dir_n_graph, prefix_dest_file + str(evidences)), 'w')
        line = file.readline()
        while line:
            match_obj = re.match("dimension: (\d+) num evidences: (\d+)", line)
            if match_obj:
                dim = int(match_obj.group(1))
                dir_n_graph = os.path.join(dest_dir, str(dim))
                if not os.path.exists(dir_n_graph):
                    os.makedirs(dir_n_graph)
                    
                evidences = int(match_obj.group(2))
                dest_file.close()
                dest_file = open(os.path.join(dir_n_graph, prefix_dest_file + str(evidences)), 'w')
                
            elif line.startswith("Time:"):
                value = float(line.replace("Time:", ""))
                dest_file.write(str(int(value))+"\n")
                
            line = file.readline()
        dest_file.close()
    
if __name__ == "__main__":
    dest_dir = "mcintyre"
    prefix_dest_file = "result_cond_mh_"
    filepath = "cond_mcintyre_prob"
    extract_timings(dest_dir, filepath, prefix_dest_file)
    prefix_dest_file = "result_caus_mh_"
    filepath = "caus_mcintyre_prob"
    extract_timings(dest_dir, filepath, prefix_dest_file)