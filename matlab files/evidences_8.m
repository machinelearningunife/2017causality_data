ht=plot(dim, cond_exact_8,'d-', dim, caus_exact_8,'s-.', ...
    dim, cond_mcintyre_8,'o-', dim, caus_mcintyre_8,'^--');
%title("8 evidences", 'FontSize', 16)
h1=legend('cond exact','caus exact','cond mcintyre','caus mcintyre')
set(h1,'FontSize',12)
set(h1, 'Box', 'off')
set(h1,'Location','NorthWest')

set(ht(1),'Color','black');
set(ht(2),'Color',[0.3 0.3 0.3]);
set(ht(3),'Color',[0.5 0.5 0.5]);
set(ht(4),'Color',[0.7 0.7 0.7]);

set(ht,'LineWidth',1);

xlabel('Number of nodes','FontWeight','bold','FontSize',14)
ylabel('Time (ms)','FontWeight','bold','FontSize',14)
ylim([0,3500])