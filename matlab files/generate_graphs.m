% change this value to have different number of evidences
evidences = 8;

read_data

% ht1 = errorbar(dim, cond_exact, cond_exact_conf, 'd', 'Color', 'black')
% hold on;
% ht2 = errorbar(dim, caus_exact, caus_exact_conf, 's', 'Color', 'blue')
% hold on;
% ht3 = errorbar(dim, cond_mcintyre, cond_mcintyre_conf, 'o', 'Color', 'green')
% hold on;
% ht4 = errorbar(dim, caus_mcintyre, caus_mcintyre_conf, '^', 'Color', 'red')
% hold on;

% ht1=semilogy(dim, cond_exact,'d', dim, caus_exact,'s', ...
%     dim, cond_mcintyre,'o', dim, caus_mcintyre,'^');
% hold on;
% Interpolation
xx = dim(1):1:dim(end);
%cond_exact_interp = spline(dim, cond_exact, xx);
cond_exact_interp = interp1(dim, cond_exact, xx, 'cubic');
% get the index of the last valid value
last_value_idx = find(cond_exact >= 0);
last_value_idx = last_value_idx(end);
last_value_idx_interp = 1 + ceil(length(xx) * (last_value_idx - 1) / (length(dim) -1));
cond_exact_interp(last_value_idx_interp:end) = NaN;

caus_exact_interp = spline(dim, caus_exact, xx);
% get the index of the last valid value
last_value_idx = find(caus_exact >= 0);
last_value_idx = last_value_idx(end);
last_value_idx_interp = 1 + ceil(length(xx) * (last_value_idx - 1) / (length(dim) -1));
caus_exact_interp((last_value_idx_interp +1):end) = NaN;

cond_mcintyre_interp = spline(dim, cond_mcintyre, xx);
% get the index of the last valid value
last_value_idx = find(cond_mcintyre >= 0);
last_value_idx = last_value_idx(end);
last_value_idx_interp = 1 + ceil(length(xx) * (last_value_idx - 1) / (length(dim) -1));
cond_mcintyre_interp((last_value_idx_interp +1):end) = NaN;

caus_mcintyre_interp = spline(dim, caus_mcintyre, xx);
% get the index of the last valid value
last_value_idx = find(caus_mcintyre >= 0);
last_value_idx = last_value_idx(end);
last_value_idx_interp = 1 + ceil(length(xx) * (last_value_idx - 1) / (length(dim) -1));
caus_mcintyre_interp((last_value_idx_interp +1):end) = NaN;


ht2=semilogy(xx, cond_exact_interp, 'd-', ...
    xx, caus_exact_interp,  's-.', ...
    xx, cond_mcintyre_interp,  'o-', ...
    xx, caus_mcintyre_interp, '^--');
% h1=legend([ht1;ht2;ht3;ht4], 'cond exact','caus exact', 'cond mcintyre', 'caus mcintyre');
% set(ht1(1),'Color','black');
set(ht2(1),'Color','black');

% set(ht1(2),'Color','blue');
set(ht2(2),'Color','blue');

% set(ht1(3),'Color','green');
set(ht2(3),'Color','green');

% set(ht1(4),'Color','red');
set(ht2(4),'Color','red');
set(ht2, 'MarkerIndices', [11,21,31,41,51,61,71,81,91,101])

h1=legend(ht2, 'cond exact','caus exact', 'cond mcintyre', 'caus mcintyre');
set(h1,'FontSize',12)
set(h1, 'Box', 'on')
set(h1,'Location','southeast')



% set(ht(2),'Color',[0.3 0.3 0.3]);
% set(ht(3),'Color',[0.5 0.5 0.5]);
% set(ht(4),'Color',[0.7 0.7 0.7]);

set(ht2,'LineWidth',1);
set(gca, 'XTick',0:10:100);

xlabel('Number of nodes','FontWeight','bold','FontSize',14)
ylabel('Time (ms)','FontWeight','bold','FontSize',14)
% if evidences == 2
%     ylim([0,250]);
% elseif evidences == 4
%     ylim([0,3500])
% elseif evidences == 6
%     ylim([0,1800])
% elseif evidences == 8
%     ylim([0,7000])
% end