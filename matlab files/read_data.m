cd ../experiments

formatSpec = '%d';
dirs = ["exact", "mcintyre"];
suffix_filename_caus = ["result_caus_", "result_caus_mh_"];
suffix_matrix_caus = ["result_caus_exact_", "result_caus_mcintyre_"];
suffix_filename_cond = ["result_cond_", "result_cond_mh_"];
suffix_matrix_cond = ["result_cond_exact_", "result_cond_mcintyre_"];

index = 1;
for dir = dirs
    cd(char(dir));
    for i = (10:10:100)
        cd(num2str(i));
        for j = (2:2:8)
%             fileID = fopen(strcat('result_caus_mh_', num2str(j)), 'r');
%             eval(['result_caus_mcintyre_' num2str(j) '_' num2str(i) '= fscanf(fileID,formatSpec)']);
%             fclose(fileID);
%             fileID = fopen(strcat('result_cond_mh_', num2str(j)), 'r');
%             eval(['result_cond_mcintyre_' num2str(j) '_' num2str(i) '= fscanf(fileID,formatSpec)']);
%             fclose(fileID);
            eval([char(suffix_matrix_caus(index)) num2str(j) '_' num2str(i) '(1:100) = NaN;']);
            eval([char(suffix_matrix_cond(index)) num2str(j) '_' num2str(i) '(1:100) = NaN;']);
            
            fileID = fopen(strcat(suffix_filename_caus(index), num2str(j)), 'r');
            if fileID ~= -1
                eval([char(suffix_matrix_caus(index)) num2str(j) '_' num2str(i) ' = fscanf(fileID,formatSpec);']);
                fclose(fileID);
            end
            
            fileID = fopen(strcat(suffix_filename_cond(index), num2str(j)), 'r');
            if fileID ~= -1
                eval([char(suffix_matrix_cond(index)) num2str(j) '_' num2str(i) ' = fscanf(fileID,formatSpec);']);
                fclose(fileID);
            end
        end
        cd ..
    end
    cd ..
    index = index + 1;
end

dim = 0:10:100;
n_queries = 10;
n_graphs = 10;
% create the arrays that will contain the averages (mu)
%cond_exact = eval(['cond_exact_' num2str(evidences) '(1:11) = NaN']);
cond_exact(1:11) = NaN;
cond_exact(1) = 0;
%eval(['cond_exact_' num2str(evidences) '(1) = 0'])
%cond_exact_conf = eval(['cond_exact_' num2str(evidences) '_conf(1:11) = NaN'])
cond_exact_conf(1:11) = NaN;
cond_exact_conf(1) = 0;
%eval(['cond_exact_' num2str(evidences) '_conf(1) = 0'])

caus_exact(1:11) = NaN;
caus_exact(1) = 0;
caus_exact_conf(1:11) = NaN;
caus_exact_conf(1) = 0;
% eval(['caus_exact_' num2str(evidences) '(1:11) = NaN'])
% eval(['caus_exact_' num2str(evidences) '(1) = 0'])
% eval(['caus_exact_' num2str(evidences) '_conf(1:11) = NaN'])
% eval(['caus_exact_' num2str(evidences) '_conf(1) = 0'])

cond_mcintyre(1:11) = NaN;
cond_mcintyre(1) = 0;
cond_mcintyre_conf(1:11) = NaN;
cond_mcintyre_conf(1) = 0;
% eval(['cond_mcintyre_' num2str(evidences) '(1:11) = NaN'])
% eval(['cond_mcintyre_' num2str(evidences) '(1) = 0'])
% eval(['cond_mcintyre_' num2str(evidences) '_conf(1:11) = NaN'])
% eval(['cond_mcintyre_' num2str(evidences) '_conf(1) = 0'])

caus_mcintyre(1:11) = NaN;
caus_mcintyre(1) = 0;
caus_mcintyre_conf(1:11) = NaN;
caus_mcintyre_conf(1) = 0;
% eval(['caus_mcintyre_' num2str(evidences) '(1:11) = NaN'])
% eval(['caus_mcintyre_' num2str(evidences) '(1) = 0'])
% eval(['caus_mcintyre_' num2str(evidences) '_conf(1:11) = NaN'])
% eval(['caus_mcintyre_' num2str(evidences) '_conf(1) = 0'])

i = 2;
for d = dim(2:end)
    result_cond_exact = eval(['result_cond_exact_' num2str(evidences) '_' num2str(d)]);
    if length(result_cond_exact) == n_graphs * n_queries
        pd = fitdist(result_cond_exact, 'Normal');
        ci = paramci(pd);
        cond_exact(i) = pd.mu;
    
    %eval(['cond_exact_' num2str(evidences) ...
    %    '(' num2str(i) ') = ' num2str(pd.mu)]);
    cond_exact_conf(i) = ci(2,1)-ci(1,1);
    %eval(['cond_exact_' num2str(evidences) '_conf' ...
    %    '(' num2str(i) ') = ' num2str(ci(2,1)-ci(1,1))]);
    end
    pd = fitdist(eval(['result_caus_exact_' num2str(evidences) '_' num2str(d)]), ...
        'Normal');
    ci = paramci(pd);
    caus_exact(i) = pd.mu;
    caus_exact_conf(i) = ci(2,1) - ci(1,1);
    %     eval(['caus_exact_' num2str(evidences) ...
    %         '(' num2str(i) ') = ' num2str(pd.mu)]);
    %     eval(['caus_exact_' num2str(evidences) '_conf' ...
    %         '(' num2str(i) ') = ' num2str(ci(2,1)-ci(1,1))]);
    
    pd = fitdist(eval(['result_cond_mcintyre_' num2str(evidences) '_' num2str(d)]), ...
        'Normal');
    ci = paramci(pd);
    cond_mcintyre(i) = pd.mu;
    cond_mcintyre_conf(i) = ci(2,1) - ci(1,1);
    
    pd = fitdist(eval(['result_caus_mcintyre_' num2str(evidences) '_' num2str(d)]), ...
        'Normal');
    ci = paramci(pd);
    caus_mcintyre(i) = pd.mu;
    caus_mcintyre_conf(i) = ci(2,1) - ci(1,1);
    i = i + 1;
end

filename = 'caus_square_errors_2-8.csv';
square_err_matrix = csvread(filename);
square_err_matrix = square_err_matrix(2:end,2:end);

cd '../matlab files'