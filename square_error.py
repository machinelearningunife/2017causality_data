import os
import re
import csv

MAX_NUM_EVIDENCE = 8
MIN_NUM_EVIDENCE = 2
STEP_NUM_EVIDENCE = 2
MAX_DIM_GRAPH = 100
STEP_DIM_GRAPH = 10
MIN_DIM_GRAPH = 10

def square_error(correct_filepath, approx_filepath):
    total = 0.0
    num = 0
    approx_file = open(approx_filepath, 'r')
    square_err_matrix = [[0.0 for _ in xrange((MAX_NUM_EVIDENCE - MIN_NUM_EVIDENCE)/2 + 1) ] 
                               for _ in xrange((MAX_DIM_GRAPH - MIN_DIM_GRAPH)/STEP_DIM_GRAPH + 1)]
    x = -1
    y = -1
    # read file that contains the exact results
    with open(correct_filepath, 'r') as correct_file:
        line = correct_file.readline()
        while line:
            match_obj = re.match("dimension: (\d+) num evidences: (\d+)", line)
            if match_obj:
                if x > -1 and y > -1:
                    square_err_matrix[x][y] = total / num
                    total = 0.0
                    num = 0
                dim = int(match_obj.group(1))
                evidences = int(match_obj.group(2))
                x = (dim - MIN_DIM_GRAPH) / STEP_DIM_GRAPH
                y = (evidences - MIN_NUM_EVIDENCE) / STEP_NUM_EVIDENCE
                print "x: %d y: %d" % (x, y)
            elif line.startswith("Prob:"):
                num += 1
                correct_value = float(line.replace("Prob:", ""))
                approx_line = approx_file.readline()
                while not approx_line.startswith("Prob:"):
                    approx_line = approx_file.readline()
                approx_value = float(approx_line.replace("Prob:", ""))   
                total += (approx_value - correct_value) ** 2
            line = correct_file.readline()
    # take into account the last values
    square_err_matrix[x][y] = total / num
    approx_file.close()
    return square_err_matrix 
    
if __name__ == "__main__":
    correct_filepath = "caus_exact_prob"
    approx_filepath = "caus_mcintyre_prob"
    square_err_matrix = square_error(correct_filepath, approx_filepath)
    # Write results on a CSV file
    dimensions = []
    for n in range(10, MAX_DIM_GRAPH + 1, STEP_DIM_GRAPH):
        dimensions.append(n)
        
    evidence_sizes = [' ']
    for ev_size in range(MIN_NUM_EVIDENCE, MAX_NUM_EVIDENCE + 1, STEP_NUM_EVIDENCE):
        evidence_sizes.append(ev_size)
    
    filename = "caus_square_errors_%d-%d.csv" % (MIN_NUM_EVIDENCE, MAX_NUM_EVIDENCE)

    with open(filename, "w") as fp:
        wr = csv.writer(fp, dialect='excel')
        wr.writerow(evidence_sizes)
        #n = 0
#            for row in result_matrices[i]:
        for row,n in zip(square_err_matrix, range(MIN_DIM_GRAPH, MAX_DIM_GRAPH + 1, STEP_DIM_GRAPH)):
            l = [n]
            l.extend(row)
            wr.writerow(l)