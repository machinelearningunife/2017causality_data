#!/bin/bash
#PBS -N causal_exact
#PBS -l walltime=24:00:00
#PBS -l select=1:ncpus=8:mem=8GB
#PBS -j eo
#PBS -A IscrC_STARAI
#PBS -m abe


cd $PBS_O_WORKDIR/..

python ./used_scripts/execute_experiments_caus_exact.py > caus_exact_prob
